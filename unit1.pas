unit Unit1;

{$mode objfpc}
{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ExtCtrls, StdCtrls,
  ComCtrls, DBGrids, DBCtrls, INIFiles, uEdit, LR_Class, LR_DBSet,
  LR_E_HTM, sqlite3conn, sqldb, db;

type

  { TForm1 }

  TForm1 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    DBGrid4: TDBGrid;
    DBText12: TDBText;
    DBText13: TDBText;
    DBText14: TDBText;
    ds4: TDataSource;
    Label15: TLabel;
    vMEMOsignature: TMemo;
    Panel4: TPanel;
    rp_ranks_year: TSQLQuery;
    SearchDown: TButton;
    Button4: TButton;
    Button5: TButton;
    Button6: TButton;
    Button7: TButton;
    SearchOFF: TButton;
    DBGrid2: TDBGrid;
    DBGrid3: TDBGrid;
    ds2: TDataSource;
    ds3: TDataSource;
    SearchEdit: TEdit;
    Panel1: TPanel;
    Panel3: TPanel;
    rp_ranks: TSQLQuery;
    DBGrid1: TDBGrid;
    DBMemo1: TDBMemo;
    DBMemo2: TDBMemo;
    DBText1: TDBText;
    DBText10: TDBText;
    DBText11: TDBText;
    DBText2: TDBText;
    DBText3: TDBText;
    DBText4: TDBText;
    DBText5: TDBText;
    DBText6: TDBText;
    DBText7: TDBText;
    DBText8: TDBText;
    DBText9: TDBText;
    ds1: TDataSource;
    frDBDataSet1: TfrDBDataSet;
    frHTMExport1: TfrHTMExport;
    frReport1: TfrReport;
    Label1: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label2: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Memo2: TMemo;
    Memo3: TMemo;
    Memo4: TMemo;
    Memo5: TMemo;
    Memo6: TMemo;
    Memo7: TMemo;
    Memo1: TMemo;
    Memo8: TMemo;
    PageControl1: TPageControl;
    Panel2: TPanel;
    rp_otdel: TSQLQuery;
    chk_column: TSQLQuery;
    SQLiteCon: TSQLite3Connection;
    TabSheet5: TTabSheet;
    tProto: TSQLQuery;
    trans: TSQLTransaction;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure Button7Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure frReport1GetValue(const ParName: String; var ParValue: Variant);
    procedure SearchDownClick(Sender: TObject);
    procedure SearchEditKeyPress(Sender: TObject; var Key: char);
    procedure SearchOFFClick(Sender: TObject);
  private

  public

  end;

var
  Form1: TForm1;
  IniF: TIniFile;// Класс для работы с INI-файлами
  iProtNUM, vINTERNAL_ID: integer;
  vCurMedic, vCurSyster: integer;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.Button4Click(Sender: TObject);
begin
    frReport1.LoadFromFile(ExtractFileDir(ParamStr(0)) +
        DirectorySeparator + 'proto.lrf');
    frReport1.PrepareReport;
    frReport1.ShowPreparedReport;
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
    if rp_ranks.Active then rp_ranks.close;
    rp_ranks.open;
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
    if rp_otdel.Active then rp_otdel.close;
    rp_otdel.open;
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
  if rp_ranks_year.Active then rp_ranks_year.close;
  rp_ranks_year.open;
end;

procedure TForm1.Button5Click(Sender: TObject);
var
   v_sql: string;
begin

    fEdit := TfEdit.Create(self);
    //fEdit.MaskEdit1.EditText := tproto.FieldByName('id').AsString;
    fEdit.MaskEdit1.EditText := tproto.FieldByName('print_id').AsString;

    //fEdit.MaskEdit1.ReadOnly:=true;
    fEdit.MaskEdit1.Color:=clLime;
    fedit.DateEdit1.Text:=tproto.FieldByName('date').AsString;
    fEdit.Caption := 'Протокол № ' + fEdit.MaskEdit1.EditText +
      ' от ' + fedit.DateEdit1.Text;
    fEdit.Memo1.Lines.Text:=tproto.FieldByName('result').AsString;
    fEdit.Memo2.Lines.Text:=tproto.FieldByName('conclusion').AsString;

    fEdit.Memo3.Lines.Text:=tproto.FieldByName('signeetype').AsString;
    fEdit.Memo4.Lines.Text:=tproto.FieldByName('signers').AsString;

    fEdit.ComboBox1.Items := memo4.Lines;
    fEdit.ComboBox3.Items := memo2.Lines;
    fEdit.ComboBox4.Items := memo1.Lines;
    fEdit.ComboBox5.Items := memo3.Lines;
    fEdit.ComboBox6.Items := memo8.Lines; 
    fEdit.cbmedic.Items := vMEMOsignature.Lines;
    fEdit.cbsyster.Items := vMEMOsignature.Lines;

    fEdit.edit1.text := tproto.FieldByName('fio').AsString;
    fEdit.edit2.text := tproto.FieldByName('age').AsString;
    fEdit.edit3.text := tproto.FieldByName('tolmud').AsString;

    fEdit.ComboBox1.Text := tproto.FieldByName('oper').AsString;
    fEdit.ComboBox2.Text := tproto.FieldByName('sex').AsString;
    fEdit.ComboBox3.Text := tproto.FieldByName('otdel').AsString;
    fEdit.ComboBox4.Text := tproto.FieldByName('rank').AsString;
    fEdit.ComboBox5.Text := tproto.FieldByName('apparatus').AsString;
    fEdit.ComboBox6.Text := tproto.FieldByName('prt_rank').AsString;  
    fEdit.cbmedic.Text := tproto.FieldByName('medic').AsString;
    fEdit.cbsyster.Text := tproto.FieldByName('syster').AsString;

    if tproto.FieldByName('biopsy').AsString = '1'
    then
        fEdit.CheckBox1.State:=cbChecked
    else
        fEdit.CheckBox1.State:=cbUnchecked;

    fEdit.ShowModal;
    if fEdit.ModalResult = mrOk then
    begin
      with fEdit do
      begin
           v_sql := 'update proto set ';                                     
           v_sql := v_sql + 'print_id = ' + QuotedStr(Trim(MaskEdit1.EditText))+', ';
           v_sql := v_sql + 'date = ' + QuotedStr(Trim(DateEdit1.Text))+', ';
           v_sql := v_sql + 'oper = ' + QuotedStr(Trim(ComboBox1.Text))+', ';
           v_sql := v_sql + 'fio = ' + QuotedStr(Trim(edit1.Text))+', ';
           v_sql := v_sql + 'age = ' + QuotedStr(Trim(Edit2.Text))+', ';
           v_sql := v_sql + 'sex = ' + QuotedStr(Trim(ComboBox2.Text))+', ';
           v_sql := v_sql + 'tolmud = ' + QuotedStr(Trim(Edit3.Text))+', ';
           v_sql := v_sql + 'otdel = ' + QuotedStr(Trim(ComboBox3.Text))+', ';
           v_sql := v_sql + 'rank = ' + QuotedStr(Trim(ComboBox4.Text))+', ';
           v_sql := v_sql + 'apparatus = ' + QuotedStr(Trim(ComboBox5.Text))+', ';  
           v_sql := v_sql + 'medic = ' + QuotedStr(Trim(cbmedic.Text))+', ';
           v_sql := v_sql + 'syster = ' + QuotedStr(Trim(cbsyster.Text))+', ';
           if CheckBox1.State = cbUnchecked
           then
               v_sql := v_sql + 'biopsy = ' + QuotedStr('0')+', '
           else
               v_sql := v_sql + 'biopsy = ' + QuotedStr('1')+', ';
           v_sql := v_sql + 'result = ' + QuotedStr(Trim(memo1.Text))+', ';
           v_sql := v_sql + 'conclusion = ' + QuotedStr(Trim(memo2.Text))+', ';
      end;
      v_sql := v_sql + 'signeetype = ' + QuotedStr(Trim(fEdit.Memo3.Text))+', ';
      v_sql := v_sql + 'signers = ' + QuotedStr(Trim(fEdit.Memo4.Text))+', ';
      v_sql := v_sql + 'prt_rank = ' + QuotedStr(Trim(fEdit.ComboBox6.Text));
      v_sql := v_sql + ' where id = ' + tproto.FieldByName('id').AsString;
      SQLiteCon.ExecuteDirect(v_sql);
      trans.Commit;
      iProtNUM := strtoint(Trim(fEdit.MaskEdit1.EditText));
      INiF.WriteInteger('MAIN', 'PROT_NUMBER', iProtNUM);
    end;
    fedit.Free;
    if not tProto.active then
       tProto.open;
    DBGrid1.Refresh;

end;

procedure TForm1.Button6Click(Sender: TObject);
var
  tmp_protnum: integer;
  v_sql: string;
begin
  fEdit := TfEdit.Create(self);
  tmp_protnum := iProtNUM + 1;
  fEdit.MaskEdit1.EditText := IntToStr(tmp_protnum);
  fEdit.Caption := 'Протокол № ' + IntToStr(iProtNUM + 1) +
    ' от ' + datetostr(fedit.DateEdit1.Date);
  if FileExists(ExtractFileDir(ParamStr(0)) + DirectorySeparator +
    'по_умолчанию.txt') then
    fEdit.Memo1.Lines.LoadFromFile(ExtractFileDir(ParamStr(0)) +
      DirectorySeparator + 'по_умолчанию.txt');
  if FileExists(ExtractFileDir(ParamStr(0)) + DirectorySeparator +
    'по_умолчанию_заключение.txt') then
    fEdit.Memo2.Lines.LoadFromFile(ExtractFileDir(ParamStr(0)) +
      DirectorySeparator + 'по_умолчанию_заключение.txt');
  fEdit.ComboBox1.Items := memo4.Lines;
  fEdit.ComboBox3.Items := memo2.Lines;
  fEdit.ComboBox4.Items := memo1.Lines;
  fEdit.ComboBox5.Items := memo3.Lines;
  fEdit.ComboBox6.Items := memo8.Lines;  
  fEdit.cbmedic.Items := vMEMOsignature.Lines;
  fEdit.cbsyster.Items := vMEMOsignature.Lines;


  fEdit.Memo3.Lines.Text:= memo6.text;
  fEdit.Memo4.Lines.Text:= memo7.text;

  if fEdit.ComboBox1.items.Count > 0 then
     fEdit.ComboBox1.ItemIndex := 0;
  if fEdit.ComboBox3.items.Count > 0 then
     fEdit.ComboBox3.ItemIndex := 0;
  if fEdit.ComboBox3.items.Count > 13 then
     fEdit.ComboBox3.ItemIndex := 14;
  if fEdit.ComboBox4.items.Count > 0 then
     fEdit.ComboBox4.ItemIndex := 0;
  if fEdit.ComboBox5.items.Count > 0 then
     fEdit.ComboBox5.ItemIndex := 0;
  if fEdit.ComboBox6.items.Count > 0 then
     fEdit.ComboBox6.ItemIndex := 0;      
  if fEdit.cbmedic.items.Count > 0 then
     fEdit.cbmedic.ItemIndex := vCurMedic;
  if fEdit.cbsyster.items.Count > 0 then
     fEdit.cbsyster.ItemIndex := vCurSyster;
  fEdit.ShowModal;
  if fEdit.ModalResult = mrOk then
  begin
    vINTERNAL_ID := vINTERNAL_ID + 1;
    with fEdit do
    begin
         v_sql := 'insert into proto(id,medic,syster,print_id,date,oper,fio,age,sex,tolmud,otdel,rank,apparatus,biopsy,result,conclusion,signeetype,signers,prt_rank) ';
         v_sql := v_sql + 'values (';
         v_sql := v_sql + QuotedStr(inttostr(vINTERNAL_ID))+', '; 
         v_sql := v_sql + QuotedStr(Trim(cbmedic.Text))+', ';
         v_sql := v_sql + QuotedStr(Trim(cbsyster.Text))+', ';
         v_sql := v_sql + QuotedStr(Trim(MaskEdit1.Text))+', ';
         v_sql := v_sql + QuotedStr(Trim(DateEdit1.Text))+', ';
         v_sql := v_sql + QuotedStr(Trim(ComboBox1.Text))+', ';
         v_sql := v_sql + QuotedStr(Trim(edit1.Text))+', ';
         v_sql := v_sql + QuotedStr(Trim(Edit2.Text))+', ';
         v_sql := v_sql + QuotedStr(Trim(ComboBox2.Text))+', ';
         v_sql := v_sql + QuotedStr(Trim(Edit3.Text))+', ';
         v_sql := v_sql + QuotedStr(Trim(ComboBox3.Text))+', ';
         v_sql := v_sql + QuotedStr(Trim(ComboBox4.Text))+', ';
         v_sql := v_sql + QuotedStr(Trim(ComboBox5.Text))+', ';
         if CheckBox1.State = cbUnchecked
         then
             v_sql := v_sql + QuotedStr('0')+', '
         else
             v_sql := v_sql + QuotedStr('1')+', ';
         v_sql := v_sql + QuotedStr(Trim(memo1.Text))+', ';
         v_sql := v_sql + QuotedStr(Trim(memo2.Text))+', ';
         iProtNUM := strtoint(Trim(MaskEdit1.Text));
    end;
    v_sql := v_sql + QuotedStr(Trim(memo6.Text))+', ';
    v_sql := v_sql + QuotedStr(Trim(memo7.Text))+', ';
    v_sql := v_sql + QuotedStr(Trim(fEdit.ComboBox6.Text));
    v_sql := v_sql + ')';
    SQLiteCon.ExecuteDirect(v_sql);
    trans.Commit;
    vCurMedic := fEdit.cbmedic.ItemIndex;
    vCurSyster := fEdit.cbsyster.ItemIndex;
    iProtNUM := strtoint(Trim(fEdit.MaskEdit1.EditText));
    INiF.WriteInteger('MAIN', 'PROT_NUMBER', iProtNUM); 
    INiF.WriteInteger('MAIN', 'INTERNAL_ID', vINTERNAL_ID);
  end;
  fedit.Free;
  if not tProto.active then
     tProto.open;
  DBGrid1.Refresh;
end;

procedure TForm1.Button7Click(Sender: TObject);
var
  v_sql: string;
begin
  //ShowMessage('Заглушка, пока не удалять #' + tProto.FieldByName('id').AsString);
  if MessageDlg('Удаление','Действительно удалить протокол №'+tProto.FieldByName('print_id').AsString,
      mtConfirmation,[mbOK,mbCancel],0) = mrOK
  then
  begin
    v_sql := 'delete from proto ';
    v_sql := v_sql + ' where id = ' + tproto.FieldByName('id').AsString;
    SQLiteCon.ExecuteDirect(v_sql);
    trans.Commit;
    if not tProto.active then
       tProto.open;
    DBGrid1.Refresh;
  end;
end;


procedure TForm1.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
  memo1.Lines.SaveToFile(ExtractFileDir(ParamStr(0)) + DirectorySeparator +
    'звания.txt');
  memo2.Lines.SaveToFile(ExtractFileDir(ParamStr(0)) + DirectorySeparator +
    'отделения.txt');
  memo3.Lines.SaveToFile(ExtractFileDir(ParamStr(0)) + DirectorySeparator +
    'аппараты.txt');
  memo4.Lines.SaveToFile(ExtractFileDir(ParamStr(0)) + DirectorySeparator +
    'наименования.txt');
  memo5.Lines.SaveToFile(ExtractFileDir(ParamStr(0)) + DirectorySeparator +
    'госпиталь.txt');
  memo6.Lines.SaveToFile(ExtractFileDir(ParamStr(0)) + DirectorySeparator +
    'подпись_долж.txt');
  memo7.Lines.SaveToFile(ExtractFileDir(ParamStr(0)) + DirectorySeparator +
    'подпись_фио.txt');
  memo8.Lines.SaveToFile(ExtractFileDir(ParamStr(0)) + DirectorySeparator +
    'звания_печать.txt');
  vMEMOsignature.Lines.SaveToFile(ExtractFileDir(ParamStr(0)) + DirectorySeparator +
    'фио_персонала.txt');
  INiF.WriteInteger('MAIN', 'LAST_TAB', pagecontrol1.PageIndex);
  INiF.WriteInteger('MAIN', 'PROT_NUMBER', iProtNUM); 
  INiF.WriteInteger('MAIN', 'INTERNAL_ID', vINTERNAL_ID);
  tProto.Close;
  trans.Commit;
  trans.Active:= false;
  SQLiteCon.Connected:=false;
  CloseAction := caFree;
end;

procedure TForm1.FormCreate(Sender: TObject);
var
  itab: integer;
begin
  tProto.close;
  trans.Active:=false;
  SQLiteCon.Connected:=false;

  Inif := TINIFile.Create(ChangeFileExt(ParamStr(0), '.ini'));
  itab := INiF.ReadInteger('MAIN', 'LAST_TAB', 0);
  iProtNUM := INiF.ReadInteger('MAIN', 'PROT_NUMBER', 1);  
  vINTERNAL_ID := INiF.ReadInteger('MAIN', 'INTERNAL_ID', iProtNUM + 1);
  pagecontrol1.PageIndex := itab;
  //caption := inttostr(itab);
  if FileExists(ExtractFileDir(ParamStr(0)) + DirectorySeparator +
    'звания.txt') then
  memo1.Lines.LoadFromFile(ExtractFileDir(ParamStr(0)) + DirectorySeparator +
    'звания.txt');
  if FileExists(ExtractFileDir(ParamStr(0)) + DirectorySeparator +
    'отделения.txt') then
  memo2.Lines.LoadFromFile(ExtractFileDir(ParamStr(0)) + DirectorySeparator +
    'отделения.txt');
  if FileExists(ExtractFileDir(ParamStr(0)) + DirectorySeparator +
    'аппараты.txt') then
  memo3.Lines.LoadFromFile(ExtractFileDir(ParamStr(0)) + DirectorySeparator +
    'аппараты.txt');
  if FileExists(ExtractFileDir(ParamStr(0)) + DirectorySeparator +
    'наименования.txt') then
  memo4.Lines.LoadFromFile(ExtractFileDir(ParamStr(0)) + DirectorySeparator +
    'наименования.txt');
  if FileExists(ExtractFileDir(ParamStr(0)) + DirectorySeparator +
    'госпиталь.txt') then
  memo5.Lines.LoadFromFile(ExtractFileDir(ParamStr(0)) + DirectorySeparator +
    'госпиталь.txt');
  if FileExists(ExtractFileDir(ParamStr(0)) + DirectorySeparator +
    'подпись_долж.txt') then
  memo6.Lines.LoadFromFile(ExtractFileDir(ParamStr(0)) + DirectorySeparator +
    'подпись_долж.txt');
  if FileExists(ExtractFileDir(ParamStr(0)) + DirectorySeparator +
    'подпись_фио.txt') then
  memo7.Lines.LoadFromFile(ExtractFileDir(ParamStr(0)) + DirectorySeparator +
    'подпись_фио.txt');
  if FileExists(ExtractFileDir(ParamStr(0)) + DirectorySeparator +
    'звания_печать.txt') then
  memo8.Lines.LoadFromFile(ExtractFileDir(ParamStr(0)) + DirectorySeparator +
    'звания_печать.txt');
  if FileExists(ExtractFileDir(ParamStr(0)) + DirectorySeparator +
    'фио_персонала.txt') then
  vMEMOsignature.Lines.LoadFromFile(ExtractFileDir(ParamStr(0)) + DirectorySeparator +
    'фио_персонала.txt');
  SQLiteCon.DatabaseName:= ExtractFileDir(ParamStr(0)) + DirectorySeparator + 'proto.db';
  SQLiteCon.UserName := 'irina';
  SQLiteCon.Password := 'irina';

  //create db if none exists
  if not FileExists(SQLiteCon.DatabaseName) then
  begin
    SQLiteCon.CreateDB;
    //ShowMessage('created new ' + SQLiteCon.DatabaseName);
    //ShowMessage('CREATE TABLE ...');
    SQLiteCon.ExecuteDirect('CREATE TABLE if not exists proto(id INTEGER PRIMARY KEY DESC, print_id, date, oper, fio, age, sex ,tolmud, otdel, rank, apparatus, biopsy, result blob, conclusion blob, signeetype, signers, prt_rank, medic, syster)');
    //ShowMessage('CREATE unique index ui_id on proto(id)');
    SQLiteCon.ExecuteDirect('CREATE unique index if not exists ui_id on proto(id)');
    //ShowMessage('CREATE index i_otdel on proto(otdel)');
    SQLiteCon.ExecuteDirect('CREATE index if not exists i_otdel on proto(otdel)');
    //ShowMessage('CREATE index i_rank on proto(rank)');
    SQLiteCon.ExecuteDirect('CREATE index if not exists i_rank on proto(rank)');
    // добавим индекс по фио
    SQLiteCon.ExecuteDirect('CREATE index if not exists i_fio on proto(fio)');
  end;
  // connect to db
  SQLiteCon.Connected:=true;
  trans.Active:=true;

  ///////////////////////////////////// убрать потом, в сл.версии
  // обновим базу - добавим индекс по фио
  SQLiteCon.ExecuteDirect('CREATE index if not exists i_fio on proto(fio)');

  // добавим новое поле, тк в старой базе его не было
  //SELECT COUNT(*) AS count_ FROM pragma_table_info('proto') WHERE name='print_id'
  chk_column.open;
  if chk_column.FieldByName('count_').AsInteger = 0 then
     begin
       SQLiteCon.ExecuteDirect('alter table proto add print_id');
       SQLiteCon.ExecuteDirect('update proto set print_id = id where print_id is null');
       trans.Commit;
     end;
  //showmessage(chk_column.FieldByName('count_').AsString);
  chk_column.close;

  // создадим поля для врача и медсестры: medic, syster
  chk_column.SQL.Text:='SELECT COUNT(*) AS count_ FROM pragma_table_info(''proto'') WHERE name=''medic''';
  chk_column.open;
  if chk_column.FieldByName('count_').AsInteger = 0 then
       SQLiteCon.ExecuteDirect('alter table proto add medic');
  chk_column.close;
  chk_column.SQL.Text:='SELECT COUNT(*) AS count_ FROM pragma_table_info(''proto'') WHERE name=''syster''';
  chk_column.open;
  if chk_column.FieldByName('count_').AsInteger = 0 then
       SQLiteCon.ExecuteDirect('alter table proto add syster');
  chk_column.close;

  tProto.ReadOnly:= true;
  tProto.Open;
end;

procedure TForm1.frReport1GetValue(const ParName: String; var ParValue: Variant);
begin
  if ParName = 'gospital' then ParValue := memo5.text;
end;

procedure TForm1.SearchDownClick(Sender: TObject);
var
  search_term : String;
begin

  search_term := trim(SearchEdit.Text);
  if Pos('''', search_term) > 0 then
     showmessage('Кавычки запрещены')
  else
    begin
      ds1.Enabled := false;
      tproto.close;
      if search_term <> '' then
          tproto.SQL.text := 'select * from proto ' +
          'where fio like ''%' + search_term +
          '%'' order by id desc'
      else
          tproto.SQL.text := 'select * from proto order by id desc';

      //showmessage(tproto.SQL.text);

      tproto.open;
      ds1.Enabled := true;
      DBGrid1.Refresh;
    end;
  //
  //caption := ansiuppercase(ansileftstr(search_term,1));
end;

procedure TForm1.SearchEditKeyPress(Sender: TObject; var Key: char);
begin
  if key = #13 then SearchDownClick(Sender);
end;

procedure TForm1.SearchOFFClick(Sender: TObject);
begin
  SearchEdit.Text := '';
  SearchDownClick(Sender);
end;

end.
