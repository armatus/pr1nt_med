unit uEdit;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ExtCtrls, StdCtrls,
  MaskEdit, EditBtn;

type

  { TfEdit }

  TfEdit = class(TForm)
    Button1: TButton;
    Button2: TButton;
    CheckBox1: TCheckBox;
    ComboBox1: TComboBox;
    ComboBox2: TComboBox;
    ComboBox3: TComboBox;
    ComboBox4: TComboBox;
    ComboBox5: TComboBox;
    ComboBox6: TComboBox;
    cbmedic: TComboBox;
    cbsyster: TComboBox;
    DateEdit1: TDateEdit;
    Edit1: TEdit;
    Edit2: TEdit;
    Edit3: TEdit;
    Label1: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    MaskEdit1: TMaskEdit;
    Memo1: TMemo;
    Memo2: TMemo;
    Memo3: TMemo;
    Memo4: TMemo;
    Panel1: TPanel;
    procedure Button2Click(Sender: TObject);
    procedure MaskEdit1MouseLeave(Sender: TObject);
  private

  public

  end;

var
  fEdit: TfEdit;
  bChanged: boolean;

implementation

{$R *.lfm}

{ TfEdit }

procedure TfEdit.Button2Click(Sender: TObject);
begin
  Close;
end;

procedure TfEdit.MaskEdit1MouseLeave(Sender: TObject);
begin
  
  fEdit.Caption := 'Протокол № ' + MaskEdit1.EditText +
    ' от ' + datetostr(fedit.DateEdit1.Date);
end;



end.
